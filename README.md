# uRADMonitor-local

Pull data from a local uRADMonitor and publish to mqtt (for use in home automation, graphing, etc). See https://www.uradmonitor.com

I run it in cron like this:

    */10 * * * * /root/uRADMonitor.py http://192.168.1.134/j > /dev/null 2>&1

Example run:

    /root/uRADMonitor.py http://192.168.1.134/j

In mqtt it will look like this:

    home-assistant/uRADMonitor110000xx/cpm 23
    home-assistant/uRADMonitor110000xx/dose 0.15
    home-assistant/uRADMonitor110000xx/temperature 18.2
